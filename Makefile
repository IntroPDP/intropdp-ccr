all:
	$(info Please call make omp or make mpi)
omp:
	g++ -std=c++17 -fopenmp -O2 omp.cpp -o omp-test
mpi:
	mpicxx -std=c++17 -O3 mpi.cpp -o mpi-test
clean:
	rm -rf omp-test mpi-test
