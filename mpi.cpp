#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[]) {
    int size = -1;
    int rank = -1;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::cout << "Hi from rank: " << rank << " out of " << size << std::endl;

    return MPI_Finalize();
} // main
