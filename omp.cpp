#include <iostream>
#include <omp.h>

int main(int argc, char* argv[]) {
    #pragma omp parallel
    {
        #pragma omp single
        std::cout << "Running with " << omp_get_num_threads() << " threads" << std::endl;
        #pragma omp critical
        std::cout << "Hi from thread: " << omp_get_thread_num() << std::endl;
    }
    return 0;
} // main
